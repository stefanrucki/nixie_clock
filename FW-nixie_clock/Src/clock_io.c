//
// Created by Stefan on 2020-02-08.
//


#include "clock_io.h"


void clock_display_H(uint8_t H)
{
    if(H==0) H=10;
    H--;
    HAL_GPIO_WritePin(A1_GPIO_Port, A1_Pin, H>>0 & 0x01);
    HAL_GPIO_WritePin(B1_GPIO_Port, B1_Pin, H>>1 & 0x01);
    HAL_GPIO_WritePin(C1_GPIO_Port, C1_Pin, H>>2 & 0x01);
    HAL_GPIO_WritePin(D1_GPIO_Port, D1_Pin, H>>3 & 0x01);
}

void clock_display_h(uint8_t h)
{
    if(h==0) h=10;
    h--;
    HAL_GPIO_WritePin(A2_GPIO_Port, A2_Pin, h>>0 & 0x01);
    HAL_GPIO_WritePin(B2_GPIO_Port, B2_Pin, h>>1 & 0x01);
    HAL_GPIO_WritePin(C2_GPIO_Port, C2_Pin, h>>2 & 0x01);
    HAL_GPIO_WritePin(D2_GPIO_Port, D2_Pin, h>>3 & 0x01);
}

void clock_display_M(uint8_t M)
{
    if(M==0) M=10;
    M--;
    HAL_GPIO_WritePin(A3_GPIO_Port, A3_Pin, M>>0 & 0x01);
    HAL_GPIO_WritePin(B3_GPIO_Port, B3_Pin, M>>1 & 0x01);
    HAL_GPIO_WritePin(C3_GPIO_Port, C3_Pin, M>>2 & 0x01);
    HAL_GPIO_WritePin(D3_GPIO_Port, D3_Pin, M>>3 & 0x01);
}

void clock_display_m(uint8_t m)
{
    if(m==0) m=10;
    m--;
    HAL_GPIO_WritePin(A4_GPIO_Port, A4_Pin, m>>0 & 0x01);
    HAL_GPIO_WritePin(B4_GPIO_Port, B4_Pin, m>>1 & 0x01);
    HAL_GPIO_WritePin(C4_GPIO_Port, C4_Pin, m>>2 & 0x01);
    HAL_GPIO_WritePin(D4_GPIO_Port, D4_Pin, m>>3 & 0x01);
}

void clock_SS_ON()
{
    HAL_GPIO_WritePin(neon_GPIO_Port, neon_Pin, GPIO_PIN_SET);
}

void clock_SS_OFF()
{
    HAL_GPIO_WritePin(neon_GPIO_Port, neon_Pin, GPIO_PIN_RESET);
}