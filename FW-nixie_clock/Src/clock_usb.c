//
// Created by Stefan on 2020-12-03.
//

#include "clock_usb.h"
#include "clock_rtc.h"

#include "usbd_cdc_if.h"

const uint8_t ascii_number_offset = 0x30;

void usb_report(volatile struct ClockRegisterTypeDef *clock_register)
{

    uint8_t report_string[10] = "--:--.--\n\r";
    report_string[0] = clock_register->time.H+ascii_number_offset;
    report_string[1] = clock_register->time.h+ascii_number_offset;
    report_string[3] = clock_register->time.M+ascii_number_offset;
    report_string[4] = clock_register->time.m+ascii_number_offset;
    report_string[6] = clock_register->time.S+ascii_number_offset;
    report_string[7] = clock_register->time.s+ascii_number_offset;

    CDC_Transmit_FS(report_string, 10);

}

void clock_usb_receive_handler(uint8_t* Buf, uint32_t *Len)
{
    extern volatile struct ClockRegisterTypeDef clock_register;

    if(Buf[0]=='s'&& *Len==7)
    {
        // s: time setting command
        // example: "s153412" sets time to 15:34:12

        clock_register.time.ST = 1;
        clock_register.time.VBATEN = 1;
        clock_register.time.OSCRUN = 1;
        clock_register.time.H=Buf[1]-ascii_number_offset;
        clock_register.time.h=Buf[2]-ascii_number_offset;
        clock_register.time.M=Buf[3]-ascii_number_offset;
        clock_register.time.m=Buf[4]-ascii_number_offset;
        clock_register.time.S=Buf[5]-ascii_number_offset;
        clock_register.time.s=Buf[6]-ascii_number_offset;

        clock_rtc_set_time(&(clock_register.time));
    }


}