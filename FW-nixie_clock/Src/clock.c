//
// Created by Stefan on 2020-02-08.
//


#include <i2c.h>
#include "clock.h"
#include "clock_rtc.h"

void clock_main_machine(volatile struct ClockRegisterTypeDef *clock_register)
{
    static uint8_t blik_cnt;
    switch (clock_register->state)
    {
        case TIME:
            // replace with clock_rtc_get_time
            HAL_I2C_Mem_Read_IT(
                    &hi2c1,
                    RTC_ADDRESS,
                    RTC_TIME_ADDRESS,
                    I2C_MEMADD_SIZE_8BIT,
                    (uint8_t*)&(clock_register->time),
                    RTC_TIME_LENGHTH)
                    ;
            // Next action is triggered with HAL_I2C_MemRxCpltCallback interrupt
            break;

        case SETH:
            if((blik_cnt/2)%2)
            {
                clock_display_H(NUMBER_OFF);
                clock_display_h(NUMBER_OFF);
            }
            else
            {
                clock_SS_ON();
                clock_display_H(clock_register->time.H);
                clock_display_h(clock_register->time.h);
            }
            blik_cnt++;
            break;

        case SETM:
            if((blik_cnt/2)%2)
            {
                clock_display_M(NUMBER_OFF);
            }
            else
            {
                clock_display_H(clock_register->time.H);
                clock_display_h(clock_register->time.h);
                clock_display_M(clock_register->time.M);
            }
            blik_cnt++;
            break;
        case SETm:
            if((blik_cnt/2)%2)
            {
                clock_display_m(NUMBER_OFF);
            }
            else
            {
                clock_display_M(clock_register->time.M);
                clock_display_m(clock_register->time.m);
            }
            blik_cnt++;

            break;
        case TEMP_ERROR:
            break;
    }
}

void sw1_handler(volatile struct ClockRegisterTypeDef *clock_register)
{
    // "Select" switch handler
    switch (clock_register->state)
    {
        case TIME:
            clock_register->state = SETH;
            break;
        case SETH:
            clock_register->state = SETM;
            break;
        case SETM:
            clock_register->state = SETm;
            break;
        case SETm:
            // replace with
            // Update time in RTC
            clock_register->time.ST = 1;
            clock_register->time.VBATEN = 1;
            clock_register->time.OSCRUN = 1;
            HAL_I2C_Mem_Write_IT(
                    &hi2c1,
                    RTC_ADDRESS,
                    RTC_TIME_ADDRESS,
                    I2C_MEMADD_SIZE_8BIT,
                    (uint8_t*)&(clock_register->time),
                    RTC_TIME_LENGHTH)
                    ;
            break;
        case TEMP_ERROR:
            break;
    }
}

void sw2_handler(volatile struct ClockRegisterTypeDef *clock_register)
{
    // "increment" switch handler
    int tmp;
    switch (clock_register->state)
    {
        case TIME:

            break;

        case SETH:
            tmp = 10*(clock_register->time.H)+(clock_register->time.h);
            tmp++;
            tmp%=24;
            clock_register->time.H = tmp/10;
            clock_register->time.h = tmp%10;
            break;

        case SETM:
            tmp = clock_register->time.M;
            tmp++;
            tmp%=6;
            clock_register->time.M = tmp;
            break;

        case SETm:
            tmp = clock_register->time.m;
            tmp++;
            tmp%=10;
            clock_register->time.m = tmp;
            break;

        case TEMP_ERROR:
            break;
    }
}

void clock_display_time(volatile struct TimeTypeDef *time)
{
    // seconds indicator blinking
    if(time->s % 2)
    {
        clock_SS_OFF();
    }
    else
    {
        clock_SS_ON();
    }

    // Display time on nixie lamps
    clock_display_H(time->H);
    clock_display_h(time->h);
    clock_display_M(time->M);
    clock_display_m(time->m);
}

void clock_off()
{
    clock_SS_OFF();
    clock_display_H(NUMBER_OFF);
    clock_display_h(NUMBER_OFF);
    clock_display_M(NUMBER_OFF);
    clock_display_m(NUMBER_OFF);
}