//
// Created by Stefan on 2020-02-08.
//
#include <stdlib.h>
#include <main.h>

#include "clock.h"
#include "clock_interrupts.h"
#include "clock_usb.h"

#include "adc.h"
#include "tim.h"

/*
 * Program flow:
 *
 * timer1 -> ADC -> state machine:  [TIME] -> I2C interrupt machine -> state==TIME? -> time
 *                                  [SETHH] -> HH blink machine -> change state on button interrupt
 *                                  [SETM] -> M blink machine -> change state on button interrupt
 *                                  [SETm] -> m blink machine -> change state on button interrupt
 *                                  [TEMPERR] -> not implemented
 *
 * SW1 -> change state -> jump to state
 *
 * SW2 -> check state -> change value -> update value
 *
 */



volatile struct ClockRegisterTypeDef clock_register;

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef * htim)
{
    static uint8_t sw1_previous_state, sw2_previous_state
    ;
    // Timer loop
    if(htim == &htim1)
    {
        //HAL_ADC_Start_IT(&hadc1);     // mcu internal temperature
        HAL_ADC_Start_IT(&hadc2);       // temperature sensor
    }

    // SW1 debounce
    if(htim == &htim2)
    {

        HAL_TIM_Base_Stop_IT(&htim2);
        clock_register.sw1_lock = false;
        if(!HAL_GPIO_ReadPin(SW1_GPIO_Port, SW1_Pin) && sw1_previous_state != HAL_GPIO_ReadPin(SW1_GPIO_Port, SW1_Pin))   // if button pressed
        {
            sw1_handler(&clock_register);
        }
        sw1_previous_state = HAL_GPIO_ReadPin(SW1_GPIO_Port, SW1_Pin);

    }

    // SW2 debounce
    if(htim == &htim3)
    {
        HAL_TIM_Base_Stop_IT(&htim3);
        clock_register.sw2_lock = false;
        if(!HAL_GPIO_ReadPin(SW2_GPIO_Port, SW2_Pin) && sw2_previous_state != HAL_GPIO_ReadPin(SW2_GPIO_Port, SW2_Pin))   // if button pressed
        {
            sw2_handler(&clock_register);
        }
        sw2_previous_state = HAL_GPIO_ReadPin(SW2_GPIO_Port, SW2_Pin);
    }
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc)
{
    // Temperature measurement completed
    if(hadc == &hadc2)
    {
        clock_register.temperature = HAL_ADC_GetValue(&hadc2);
        // Check if temperature OK
        clock_main_machine(&clock_register);
    }

}

void HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    // Received time data from RTC
    if(clock_register.state == TIME)
    {
        clock_display_time(&(clock_register.time));
    }

    usb_report(&clock_register);

}

void HAL_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    // Finished updating time in RTC
    clock_register.state = TIME;
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if(GPIO_Pin == SW1_Pin && !clock_register.sw1_lock)
    {
        clock_register.sw1_lock = true;
        TIM2->CNT = 0;
        HAL_TIM_Base_Start_IT(&htim2);
    }
    if(GPIO_Pin == SW2_Pin && !clock_register.sw2_lock)
    {
        clock_register.sw2_lock = true;
        TIM3->CNT = 0;
        HAL_TIM_Base_Start_IT(&htim3);
    }
}
