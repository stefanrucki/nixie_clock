//
// Created by Stefan on 2020-12-03.
//

#include "clock_rtc.h"
#include "i2c.h"

void clock_rtc_get_time(volatile struct TimeTypeDef *clock_time_register)
{

    HAL_I2C_Mem_Read_IT(
            &hi2c1,
            RTC_ADDRESS,
            RTC_TIME_ADDRESS,
            I2C_MEMADD_SIZE_8BIT,
            (uint8_t*)(clock_time_register),
            RTC_TIME_LENGHTH)
            ;

}

void clock_rtc_set_time(volatile struct TimeTypeDef *clock_time_register)
{

    // Update time in RTC
    clock_time_register->ST = 1;
    clock_time_register->VBATEN = 1;
    clock_time_register->OSCRUN = 1;
    HAL_I2C_Mem_Write_IT(
            &hi2c1,
            RTC_ADDRESS,
            RTC_TIME_ADDRESS,
            I2C_MEMADD_SIZE_8BIT,
            (uint8_t*)(clock_time_register),
            RTC_TIME_LENGHTH)
            ;

}