//
// Created by Stefan on 2020-02-08.
//

#ifndef FW_NIXIE_CLOCK_CLOCK_IO_H
#define FW_NIXIE_CLOCK_CLOCK_IO_H

#include <stdint.h>
#include <main.h>

#define NUMBER_OFF 11

void clock_display_H(uint8_t H);
void clock_display_h(uint8_t h);
void clock_display_M(uint8_t M);
void clock_display_m(uint8_t m);
void clock_SS_ON();
void clock_SS_OFF();

#endif //FW_NIXIE_CLOCK_CLOCK_IO_H
