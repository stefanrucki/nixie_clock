//
// Created by Stefan on 2020-02-08.
//

#ifndef FW_NIXIE_CLOCK_CLOCK_H
#define FW_NIXIE_CLOCK_CLOCK_H

#include <stdint.h>
#include <stdbool.h>
#include "clock_io.h"

#define USB_DEBUG

typedef volatile enum {TIME, SETH, SETM, SETm, TEMP_ERROR} ClockMachineStateTypeDef;
struct __packed TimeTypeDef
{
    volatile unsigned char s : 4;
    volatile unsigned char S : 3;
    volatile unsigned char ST : 1;
    volatile unsigned char m : 4;
    volatile unsigned char M : 4;
    volatile unsigned char h : 4;
    volatile unsigned char H : 4;
    volatile unsigned char W : 3;
    volatile unsigned char VBATEN : 1;
    volatile unsigned char PWRFAIL : 1;
    volatile unsigned char OSCRUN : 1;
    volatile unsigned char padding1 : 2;
};

struct ClockRegisterTypeDef{

    volatile ClockMachineStateTypeDef state;
    volatile struct TimeTypeDef time;
    volatile float mcu_temperature, temperature;
    volatile bool sw1_lock;
    volatile bool sw2_lock;

};

void clock_main_machine(volatile struct ClockRegisterTypeDef *clock_register);
void clock_display_time(volatile struct TimeTypeDef *time);
void sw1_handler(volatile struct ClockRegisterTypeDef *clock_register);
void sw2_handler(volatile struct ClockRegisterTypeDef *clock_register);
void clock_off();

#endif //FW_NIXIE_CLOCK_CLOCK_H
