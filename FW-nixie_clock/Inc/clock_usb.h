//
// Created by Stefan on 2020-12-03.
//

#ifndef FW_NIXIE_CLOCK_CLOCK_USB_H
#define FW_NIXIE_CLOCK_CLOCK_USB_H

#include "clock.h"

void usb_report(volatile struct ClockRegisterTypeDef *clock_register);
void clock_usb_receive_handler(uint8_t* Buf, uint32_t *Len);

#endif //FW_NIXIE_CLOCK_CLOCK_USB_H
