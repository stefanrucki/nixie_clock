//
// Created by Stefan on 2020-02-08.
//

#ifndef FW_NIXIE_CLOCK_CLOCK_RTC_H
#define FW_NIXIE_CLOCK_CLOCK_RTC_H

#include <stdint.h>
#include "clock.h"

#define RTC_ADDRESS         0xDE
#define RTC_TIME_ADDRESS    0x00
#define RTC_TIME_LENGHTH    4      //Bytes to read from rtc register

void clock_rtc_get_time(volatile struct TimeTypeDef *clock_time_register);
void clock_rtc_set_time(volatile struct TimeTypeDef *clock_time_register);


#endif //FW_NIXIE_CLOCK_CLOCK_RTC_H
